CREATE TABLE specialties (
  id_specialite INT AUTO_INCREMENT PRIMARY KEY,
  name_specialite VARCHAR(250) NOT NULL
);

CREATE TABLE cabinets (
  id_cabinet INT AUTO_INCREMENT PRIMARY KEY,
  address VARCHAR(250) NOT NULL,
  phone VARCHAR(25) NOT NULL
);

CREATE TABLE doctors (
  id_doc INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(25) NOT NULL,
  email VARCHAR(25) NOT NULL,
  phone VARCHAR(25) REFERENCES cabinets(phone),
  specialite VARCHAR(250) REFERENCES Specialties(name_specialite),
  adress_cabinet VARCHAR(250) REFERENCES Cabinets(address)
);

CREATE TABLE patients (
  id_patient INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(25) NOT NULL,
  email VARCHAR(50) NOT NULL,
  phone VARCHAR(25) NOT NULL
);



CREATE TABLE appointments (
  id_Appointments INT AUTO_INCREMENT PRIMARY KEY,
  id_doc INT NOT NULL,
  id_patient INT NOT NULL,
  appointment_result text,
  appointment_date DATETIME NOT NULL,
  FOREIGN KEY (id_doc) REFERENCES Doctors(id_doc),
  FOREIGN KEY (id_patient) REFERENCES Patients(id_patient)
);

CREATE TABLE availability (
  id_availability INT AUTO_INCREMENT PRIMARY KEY,
  id_doc INT NOT NULL,
  start_time TIME NOT NULL,
  end_time TIME NOT NULL,
  day_of_week ENUM('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday') NOT NULL,
  FOREIGN KEY (id_doc) REFERENCES Doctors(id_doc)
);

